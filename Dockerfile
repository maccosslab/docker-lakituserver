FROM continuumio/miniconda:4.5.4 AS base
MAINTAINER Austin Keller

SHELL ["/bin/bash", "-c"]

# Add a non-root user to run the app and create home directory to store config files
RUN groupadd -r appuser && useradd -r -m -g appuser appuser

# Create conda and python setup directories and a working directory
RUN mkdir /app /app/wd && chmod -R 777 /app/wd

# Create the conda environment and make it
ADD environment.yml /tmp/environment.yml
RUN ["conda", "env", "create", "-f", "/tmp/environment.yml"]
RUN echo "source activate env" > ~/.bashrc
ENV PATH /opt/conda/envs/lakitu/bin:$PATH

# Build from swagger-codegen
FROM base AS builder

RUN apt-get update && apt-get install -y \
 default-jre \
&& rm -rf /var/lib/apt/lists/*

ENV BITBUCKET_COMMIT f8f2078310a630baa8b832c847c758261f7be9ba
ENV BITBUCKET_COMMIT_SHORT f8f2078310a6
RUN mkdir -p /app/connexion_src
ADD https://bitbucket.org/maccosslab/lakitu-connexion/get/$BITBUCKET_COMMIT.tar.gz /app/connexion_src
WORKDIR /app/connexion_src
RUN tar -xzf $BITBUCKET_COMMIT.tar.gz \
 && rm $BITBUCKET_COMMIT.tar.gz
WORKDIR /app/connexion_src/maccosslab-lakitu-connexion-${BITBUCKET_COMMIT_SHORT}/codegen
RUN ./build.sh server
RUN mv /app/connexion_src/maccosslab-lakitu-connexion-${BITBUCKET_COMMIT_SHORT}/ /app/lakitu-connexion

FROM base

COPY --from=builder /app/lakitu-connexion /app/lakitu-connexion

ENV PYTHONPATH "${PYTHONPATH}:/app/lakitu-connexion/src-gen"

RUN pip install connexion

# Configure lakitu
COPY lakitu_config_files /home/appuser/.lakitu
RUN mkdir /home/appuser/.lakitu/envs && chmod -R 777 /home/appuser/.lakitu && chown -R appuser:appuser /home/appuser/.lakitu

WORKDIR /app/wd

USER appuser

EXPOSE 5002

CMD ["python", "-m", "swagger_server"]
